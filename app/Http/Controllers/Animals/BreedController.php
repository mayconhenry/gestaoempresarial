<?php

namespace App\Http\Controllers\Animals;

use App\Models\Breed;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class BreedController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function search(Request $request, Breed $breed)
    {
        $breeds = $breed->searchForSpecie($request->id_specie);
        return response()->json($breeds, 200);
    }
}
