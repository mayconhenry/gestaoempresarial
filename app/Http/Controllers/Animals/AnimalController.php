<?php

namespace App\Http\Controllers\Animals;

use App\Models\Breed;
use App\Models\Minutes;
use App\Models\People;
use App\Models\Schedule;
use App\Models\Service;
use App\Models\ServiceType;
use App\Models\Species;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Animals;
use App\Models\MedicalRecord;
use Carbon\Carbon;

class AnimalController extends Controller
{
    const ID_SEX_MACHO = 1;
    const ID_SEX_FEMEA = 2;
    const ID_SEX_INDEFINIDO = 3;

    public function __construct()
    {
        $this->middleware('auth');
    }

    // carrega pagina de formulario para cadastrar novo animal
    public function form (Species $specie, Breed $breed, People $mPeople)
    {
        $arrSpecies = $specie->fetchAll();
        $arrBreeds = $breed->fetchAll();
        $arrClient = $mPeople->fetchAll();
        return view('animals.form', compact('arrSpecies', 'arrBreeds', 'arrClient'));
    }


    // salva um registro de um animal
    public function save (Request $request, Animals $mAnimals)
    {
        $animal['id_owner']     = $request->id_owner;
        $animal['st_name']      = $request->st_name;
        $animal['bo_alive']     = $request->bo_alive;
        $animal['st_pedigree']  = $request->st_pedigree;
        $animal['st_chip']      = $request->st_chip;
        $animal['id_specie']    = $request->id_specie;
        $animal['id_breed']     = $request->id_breed;
        $animal['st_postage']   = $request->st_postage;
        $animal['nu_weight']    = $request->nu_weight;
        $animal['id_sexo']      = $request->id_sexo;
        $animal['bo_castrated'] = $request->bo_castrated;
        $animal['st_color']     = $request->st_color;
        $animal['st_coat']      = $request->st_coat;
        $animal['dt_birth']     = $request->dt_birth;

        if ($request->id) {
            $animal['id'] = $request->id;
            $response = $mAnimals->edit($animal);
        } else {
            $response = $mAnimals->register($animal);
        }

        if ($response['success'])
            return redirect()
                ->route('animal.list')
                ->with('success', $response['message']);

        return redirect()
            ->route('animal.form')
            ->with('error', $response['message']);
    }


    // lista os animais de um usuário
    public function list (Animals $animals)
    {
        $arrAnimal = $animals->fetchAll();
        return view('animals.list', compact('arrAnimal'));
    }

    // edita os dados de um registro de um animal
    public function edit ($id = null, Species $mSpecie, Breed $mBreed, Animals $mAnimals, People $mPeople) {

        $arrSpecies = $mSpecie->fetchAll();
        $arrBreeds = $mBreed->fetchAll();
        $arrClient = $mPeople->fetchAll();
        $animal = $mAnimals->fetch($id);
        $animal->dt_birth = Carbon::parse($animal->dt_birth)->format('d/m/Y');

        return view('animals.edit', compact('arrSpecies', 'arrBreeds', 'animal', 'arrClient'));
    }

    public function detail (
                            $id = null,
                            Animals $mAnimals,
                            Service $mService,
                            ServiceType $mServiceType,
                            Schedule $mSchedule ,
                            MedicalRecord $medicalRecord,
                            Minutes $minutes
    ) {

        //Service
        $arrService = $mService->fetchAll($id)->toArray();

        //Schedule
        $arrSchedule = $mSchedule->fetchAll($id)->toArray();
        $arrServiceType = $mServiceType->fetchAll()->toArray();

        //Medical record
        $medicalRecord = $medicalRecord->fetch($id);

        //Minutes
        $arrMinutes = $minutes->fetchAll($medicalRecord->id);

        $animal = $mAnimals->fetch($id);
        return view('animals.animal', compact(
                                            'animal',
                                            'arrServiceType',
                                            'arrService','arrSchedule',
                                            'id',
                                            'medicalRecord',
                                            'arrMinutes'));
    }
}
