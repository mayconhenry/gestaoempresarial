<?php

namespace App\Http\Controllers\Animals;

use App\Models\Service;
use App\Models\ServiceType;
use App\Models\Schedule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ScheduleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index ($idAnimal = null, ServiceType $mServiceType, Schedule $mSchedule)
    {
        $arrSchedule = $mSchedule->fetchAll($idAnimal)->toArray();
        $arrServiceType = $mServiceType->fetchAll()->toArray();
        return view('animals.schedule', compact('arrServiceType', 'arrSchedule', 'idAnimal'));
    }

    // registra agendamento de atendimento
    public function save ($id = null, Request $request, Schedule $mSchedule)
    {

        $schedule['id_animal']       = $id;
        $schedule['id_type']         = $request->id_type;
        $schedule['st_note']         = $request->st_note;
        $schedule['dt_service']      = $request->dt_service;
        $schedule['bo_notification'] = $request->bo_notification ? true : false;
        $schedule['in_ativo']        = true;

        $mSchedule->register($schedule);
    }

    // apaga um lembrete de um animal
    public function delete ($id, Schedule $mSchedule)
    {
        $mSchedule->deleteSchedule($id);

    }

    // arquiva o atendimento
    public function archive ($id = null, Schedule $mSchedule, Service $mService)
    {
        $schedule = $mSchedule->fetch($id);
        $params['id_animal']  = $schedule->id_animal;
        $params['id_type']    = $schedule->id_type;
        $params['st_note']    = $schedule->st_note;
        $mService->register($params);
        $mSchedule->archiveSchedule($id);
    }


    public function listAll(Schedule $mSchedule)
    {
        $arrSchedule = $mSchedule->listAll();
        return view('animals.allSchedule', compact('arrSchedule'));
    }

    public function refreshListAll(Schedule $mSchedule)
    {
        $arrSchedule = $mSchedule->listAll();
        return response()->json($arrSchedule, 200);
    }

    public function listSchedule($id, Schedule $mSchedule)
    {
        $arrSchedule = $mSchedule->fetchAll($id)->toArray();
        return response()->json($arrSchedule, 200);
    }

}
