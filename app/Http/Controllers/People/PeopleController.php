<?php

namespace App\Http\Controllers\People;

use App\Models\City;
use App\Models\People;
use App\Models\State;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PeopleController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    // lista clientes da organizacao
    public function list (People $mPeople) {
        $arrClient = $mPeople->fetchAll();
        return view('people.list', compact('arrClient'));
    }

    // formulario para cadastrar nova pessoa
    public function form (City $city, State $state) {
        $arrState = $state->fetchAll();
        $arrCity = $city->fetchAll();
        return view('people.form', compact('arrCity', 'arrState'));
    }

    // salva cadastro de pessoa
    public function save (Request $request, People $mPeople) {

        $person = [
            'st_name'   => $request->st_name,
            'nu_phone'   => $request->nu_phone,
            'co_cpf'    => $request->co_cpf,
            'id_sex'    => $request->id_sex,
            'id_state'  => $request->id_state,
            'id_city'   => $request->id_city,
            'st_address'=> $request->st_address
        ];


        if ($request->id) {
            $person['id'] = $request->id;
            $response = $mPeople->edit($person);
        } else {
            $response = $mPeople->register($person);
        }

        if ($response['success'])
            return redirect()
                ->route('people.list')
                ->with('success', $response['message']);


        return redirect()
            ->route('people.list')
            ->with('error', $response['message']);
    }
}
