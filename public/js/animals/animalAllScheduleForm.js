$(document).ready(function() {
    function getAllShedule () {
        $.get('/animal/allSchedule', function(response) {

            var tr = '';
            $.each(response, function(i,e) {
                var date = e.dt_service.split('-').reverse().join('/');
                var bo_notification = '';
                if (e.bo_notification) {
                    bo_notification = '<i class="ni ni-chat-round ni-2x text-info"></i>';
                }

                tr +=   '<tr>' +
                            '<td class="text-center">'+bo_notification+'</td>'+
                            '<td>'+date+'</td>'+
                            '<td>'+e.no_animal+'</td>' +
                            '<td>'+e.no_type+'</td>' +
                            '<td>'+e.st_note+'</td>' +
                            '<td class="text-center">' +
                                '<a data-toggle="modal" data-target="#contact-'+e.id+'">'+
                                    '<i class="fa fa-address-card text-primary ni-2x"></i>'+
                                '</a>'+
                            '</td>'+
                            '<td class="text-center">'+
                                '<a class="archive-modal" data-id="'+e.id+'">' +
                                    '<i class="ni ni-check-bold text-success ni-2x"></i>' +
                                '</a>'+
                            '</td>' +
                            '<td class="text-center">'+
                                '<a class="delete-modal" data-id="'+e.id+'">' +
                                    '<i class="ni ni-fat-remove text-danger ni-2x"></i>' +
                                '</a>'+
                            '</td>' +

                            '<div class="modal fade" id="contact-'+e.id+'">' +
                                '<div class="modal-dialog modal-dialog-centered" >' +
                                    '<div class="modal-content">' +
                                        '<div class="modal-header">' +
                                            '<div class="col-md-10" style="margin-top: 17px;">' +
                                            '<div class="col-md-12">' +
                                                '<label>Responsável</label>' +
                                                '<h2 style="margin-top: -10px;">{{$schedule->no_owner}}</h2>' +
                                            '</div>' +
                                            '<div class="col-md-12">' +
                                                '<label>Telefone:</label>' +
                                                '<h2 style="margin-top: -10px;">{{$schedule->nu_phone}}</h2>' +
                                            '</div>' +
                                        '</div>' +
                                        '<div class="col-md-2 text-right">' +
                                            '<a href="" data-dismiss="modal" style="color: black; font-size: 30px">x</a>' +
                                            '</div>' +
                                            '</div>' +

                                            '<div class="modal-footer">' +
                                            '<a href="#" class="btn btn-info btn-sm">Cadastro Completo</a>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>'
            });

            $('#animal-schedules').html(tr);

            $('.archive-modal').off('click').on('click', function() {
                console.log('aqui');
                $('input[name=archive_schedule]').val($(this).attr('data-id'));
                $('#schedule-archive-modal').modal();
            });

            $('.delete-modal').off('click').on('click', function() {
                $('input[name=delete_schedule]').val($(this).attr('data-id'));
                $('#schedule-delete-modal').modal();
            });
        });
    }

    $('.archive-modal').on('click', function() {
        $('input[name=archive_schedule]').val($(this).attr('data-id'));
        $('#schedule-archive-modal').modal();
    });


    $('#button-archive-schedule').on('click', function (e) {
        e.preventDefault();
        var data = {
            "_token": $('meta[name="csrf-token"]').attr('content')
        };

        $.ajax({
            type: 'put',
            url: '/schedule/archive/'+$('input[name=archive_schedule]').val(),
            data: data,
            success: function () {
                getAllShedule($('#id_animal').val());
            }
        });
    });

    $('.delete-modal').on('click', function() {
        $('input[name=delete_schedule]').val($(this).attr('data-id'));
        $('#schedule-delete-modal').modal();
    });

    $('#button-delete-schedule').on('click', function (e) {
        e.preventDefault();
        var data = {
            "_token": $('meta[name="csrf-token"]').attr('content')
        };
        $.ajax({
            type: 'put',
            url: '/schedule/delete/'+$('input[name=delete_schedule]').val(),
            data: data,
            success: function () {
                getAllShedule($('#id_animal').val());
            }
        });
    });
});
