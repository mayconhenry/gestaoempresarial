$(document).ready(function() {
    $('#form-service').on('submit', function (e) {
        e.preventDefault();
        var data = $('#form-service').serialize();
        $.ajax({
            type: 'post',
            url: '/animal/'+$('#id_animal').val()+'/services',
            data: data,
            success: function () {
                $.get('/animal/'+ $('#id_animal').val() +'/services', function(response) {
                    var tr = '';
                    $.each(response, function(i,e) {
                        var date = e.dt_service.split('-').reverse().join('/');
                        tr += '<tr><td>'+(i+1)+'</td><td>'+e.no_type+'</td><td>'+date+'</td><td>'+e.st_note+'</td>';
                    });
                    $('#animals_services').html(tr);
                });
                $('#form-service')[0].reset();
            }
        });
    });
});
