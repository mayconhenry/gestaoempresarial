$(document).ready(function() {
    $('#form-minute').on('submit', function (e) {
        e.preventDefault();
        var data = $('#form-minute').serialize();
        $.ajax({
            type: 'post',
            url: '/animal/medical-record/'+1,
            data: data,
            success: function () {
                $('#todas-atas').click();
                $.get('/animal/medical-record/'+1+'/minutes', function(response) {
                    var tr = '';
                    $.each(response, function(i,e) {
                        var date = e.dt_minute.split('-').reverse().join('/');
                        tr += '<tr><td>'+(i+1)+'</td><td>'+date+'</td><td>'+date;
                        tr += '</td><td>'+e.st_abstract+'</td><td>'+e.st_description+'</td>';
                        tr += '</td><td><a data-toggle="modal" class="open-detail-minute-modal" data-id="'+e.st_description+'"><i class="fa fa-info-circle ni-2x text-info"></i></a></td>';
                    });
                    $('#lista-atas').html(tr);

                    $('.open-detail-minute-modal').on('click', function() {
                        $('#st_description').contents().first()[0].textContent=$(this).attr('data-id');
                        $('#detail-minute-modal').modal();
                    });
                });
                $('#form-minute')[0].reset();
            }
        });
    });

    $('.open-detail-minute-modal').on('click', function() {
        $('#st_description').contents().first()[0].textContent=$(this).attr('data-id');
        $('#detail-minute-modal').modal();
    });
});