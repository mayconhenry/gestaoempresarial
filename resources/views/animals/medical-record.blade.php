{{--TAB DE PRONTUÁRIO--}}
<div class="tab-pane fade" id="tabs-icons-text-3" role="tabpanel" aria-labelledby="tabs-icons-text-3-tab">
    @if (!$medicalRecord)
        <div id="incluir-novo-prontuario">
            <div class="row">
                <div class="col-12 text-center">
                    <h2>Esse animal não possui Prontuário. Deseja incluir ?</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-center">
                    <button type="button" class="btn btn-default">Sair</button>
                    <button type="button" class="btn btn-info">Incluir</button>
                </div>
            </div>
        </div>
    @else
        <div class="row">
            <div class="nav flex-column pronturario-botoes" aria-orientation="vertical">
                <a class="nav-link active paddin-menu-prontuario" id="home" data-toggle="pill" href="#medical-record-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Inicio</a>
                <a class="nav-link paddin-menu-prontuario" data-toggle="pill" id="todas-atas" href="#listar-atas" role="tab" aria-controls="v-pills-profile" aria-selected="false">Todas Atas</a>
                <a class="nav-link paddin-menu-prontuario" data-toggle="pill" href="#cadastrar-atas" role="tab" aria-controls="v-pills-messages" aria-selected="false">Cadastrar Ata</a>
{{--                <a class="nav-link paddin-menu-prontuario" data-toggle="pill" href="#consumo-produtos" role="tab" aria-controls="v-pills-settings" aria-selected="false">Produtos consumidos</a>--}}
                <a class="nav-link paddin-menu-prontuario" data-toggle="pill" href="#editar-prontuario" role="tab" aria-controls="v-pills-settings" aria-selected="false">Editar pronturário</a>
            </div>
            <div class="tab-content" id="v-pills-tabContent" style="margin-left: 60px; width:950px">
                <div class="tab-pane show active" id="medical-record-home" role="tabpanel" aria-labelledby="v-pills-home-tab">@include('animals.medical-record-home')</div>
                <div class="tab-pane" id="listar-atas" role="tabpanel" aria-labelledby="v-pills-profile-tab">@include('animals.medical-record-atas')</div>
                <div class="tab-pane" id="cadastrar-atas" role="tabpanel" aria-labelledby="v-pills-messages-tab">@include('animals.medical-record-new-ata')</div>
                <div class="tab-pane" id="consumo-produtos" role="tabpanel" aria-labelledby="v-pills-settings-tab">@include('animals.medical-record-products')</div>
                <div class="tab-pane" id="editar-prontuario" role="tabpanel" aria-labelledby="v-pills-settings-tab">@include('animals.medical-record-edit')</div>
            </div>
        </div>
    @endif
</div>
