@extends('layouts.app')

@section('content')
    @include('animals.header', [
        'title' => __(''),
        'class' => 'col-lg-7'
    ])

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-10 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <h3 class="col-12 mb-0">{{ __('Atualizar cadastro') }}</h3>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('animal.save') }}" autocomplete="off">
                            @csrf
                            @method('patch')
                            <input type="hidden" name="id" value="{{$animal->id}}">

                            <div class="row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-name">{{ __('Nome') }}</label>
                                        <input type="text" value="{{$animal->st_name_animal}}" name="st_name" id="input-name" class="form-control form-control-alternative" placeholder="{{ __('Nome') }}" required autofocus>
                                    </div>
                                </div>
                                <div class="col-8">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-name">{{ __('Responsável') }}</label>
                                        <select name="id_owner" class="form-control">
                                            <option></option>
                                            @foreach($arrClient as $client)
                                                <option value={{$client->id}}
                                                @if($client->id == $animal->id_owner)
                                                        selected="selected"
                                                        @endif
                                                >{{$client->st_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-vivo">{{ __('Vivo') }}</label>
                                        <select name="bo_alive" class="form-control">
                                            <option value="true"
                                                @if ( $animal->bo_alive == true )
                                                selected="selected"
                                                @endif
                                            >Sim</option>
                                            <option value="false"
                                                @if ( $animal->bo_alive == false )
                                                selected="selected"
                                                @endif
                                            >Não</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-pedigree">{{ __('Pedigree') }}</label>
                                        <input type="text"  value="{{$animal->st_pedigree}}" name="st_pedigree" id="input-name" class="form-control form-control-alternative">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-chip">{{ __('Chip') }}</label>
                                        <input type="text" value="{{$animal->st_chip}}" name="st_chip" id="input-name" class="form-control form-control-alternative">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-especie">{{ __('Espécie') }}</label>
                                        <select class="form-control" id="id_specie" name="id_specie">
                                            <option></option>
                                            @foreach($arrSpecies as $specie)
                                                <option value={{$specie->id}}
                                                    @if($specie->id == $animal->id_specie)
                                                        selected="selected"
                                                    @endif
                                                >{{$specie->st_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-raca">{{ __('Raça') }}</label>
                                        <select class="form-control" value="{{$animal->id_breed}}" name="id_breed">
                                            <option></option>
                                            @foreach($arrBreeds as $breed)
                                                <option value={{$breed->id}}
                                                    @if($breed->id == $animal->id_breed)
                                                            selected="selected"
                                                    @endif
                                                >{{$breed->st_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-porte">{{ __('Porte') }}</label>
                                        <input type="text" value="{{$animal->st_postage}}" name="st_postage" id="input-porte" class="form-control form-control-alternative">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-peso">{{ __('Peso') }}</label>
                                        <input type="text" value="{{$animal->nu_weight}}" name="nu_weight" id="input-peso" class="form-control form-control-alternative">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-sexo">{{ __('Sexo') }}</label>
                                        <select name="id_sexo" class="form-control">
                                            <option></option>
                                            <option value=1
                                                @if ( $animal->id_sexo == 1 )
                                                selected="selected"
                                                @endif
                                            >Macho</option>
                                            <option value=2
                                                @if ( $animal->id_sexo == 2 )
                                                selected="selected"
                                                @endif>Fêmea</option>
                                            <option value=3
                                                @if ( $animal->id_sexo == 3 )
                                                selected="selected"
                                                @endif
                                            >Não definido</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-castrado">{{ __('Castrado') }}</label>
                                        <select name="bo_castrated" class="form-control">
                                            <option></option>
                                            <option value="true"
                                                @if ( $animal->bo_castrated == true )
                                                    selected="selected"
                                                @endif
                                            >Sim</option>
                                            <option value="false"
                                                @if ( $animal->bo_castrated == false )
                                                selected="selected"
                                                @endif
                                            >Não</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-cor">{{ __('Cor predominante') }}</label>
                                        <input type="text" value="{{$animal->st_color}}" name="st_color" id="input-cor" class="form-control form-control-alternative">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-pelo">{{ __('Pelagem') }}</label>
                                        <input type="text" value="{{$animal->st_coat}}" name="st_coat" id="input-pelo" class="form-control form-control-alternative">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-nascimento">{{ __('Data de nascimento') }}</label>
                                        <input type="text" value="{{$animal->dt_birth}}" name="dt_birth" id="input-nascimento" class="form-control form-control-alternative">
                                    </div>
                                </div>
                            </div>

                            <div class="text-right">
                                <a href="{{route('animal.detail', ['id' => $animal->id])}}" class="btn btn-danger btn-sm">{{ __('Cancelar') }}</a>
                                <button type="submit" class="btn btn-success btn-sm">{{ __('Salvar') }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
@section('extra-scripts')
    <script src="{{ asset('js/animals/animal.js') }}"></script>
@endsection
