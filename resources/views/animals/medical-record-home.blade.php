<div class="tab-pane" id="tabs-attendance" role="tabpanel">
    <div class="row space-under">
        <div class="col-md-4">
            <label>Dia da chegada</label>
            <h2>{{date('d/m/Y', strtotime($medicalRecord->dt_arrival))}}</h2>
        </div>
        <div class="col-md-4">
            <label>Hora</label>
            <h2>{{date('h:i', strtotime($medicalRecord->dt_bullish_forecast))}}</h2>
        </div>
        <div class="col-md-4">
            <label>Previsão de alta</label>
            <h2>{{date('d/m/Y', strtotime($medicalRecord->dt_bullish_forecast))}}</h2>
        </div>
    </div>
    <div class="row space-under">
        <div class="col-md-4">
            <label>Consciente</label>
            <h2>{{$medicalRecord->bo_awake ? 'Sim' : 'Não'}}</h2>
        </div>
        <div class="col-md-4">
            <label>Peso</label>
            <h2>12.35 kg</h2>
        </div>
        <div class="col-md-4">
            <label>Box</label>
            <h2>{{$medicalRecord->id_box}}</h2>
        </div>
    </div>
    <div class="row space-under">
        <div class="col-md-12">
            <label>Sintomas apresentados</label>
            <h2>{{$medicalRecord->ds_note}}</h2>
        </div>
    </div>
</div>