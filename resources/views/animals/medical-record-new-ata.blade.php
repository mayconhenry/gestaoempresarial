<div class="tab-pane fade show active" id="tabs-attendance" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">

    <h1>Cadastrar ata</h1>
    <div class="dropdown-divider"></div>
    <form id="form-minute">
        @csrf
        @method('post')

        <div class="row">
            <div class="col-3">
                <div class="form-group">
                    <label class="form-control-label" for="input-name">{{ __('Data') }}</label>
                    <input type="text" name="date" id="input-name" class="form-control form-control-alternative" required autofocus>
                </div>
            </div>
            <div class="col-3">
                <div class="form-group">
                    <label class="form-control-label" for="input-name">{{ __('Hora') }}</label>
                    <input type="text" name="time" id="input-name" class="form-control form-control-alternative" required>
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label class="form-control-label" for="input-name">{{ __('Resumo') }}</label>
                    <input type="text" name="st_abstract" id="input-name" class="form-control form-control-alternative" required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <label class="form-control-label" for="input-name">{{ __('Descrição completa') }}</label>
                    <textarea name="st_description" class="form-control rounded-0" rows="10"></textarea>
                </div>
            </div>
        </div>
        <div class="text-right">
            <a href="{{ route('animal.list') }}" class="btn btn-danger btn-sm">{{ __('Cancelar') }}</a>
            <button type="submit" class="btn btn-success btn-sm">{{ __('Salvar') }}</button>
        </div>
    </form>
</div>