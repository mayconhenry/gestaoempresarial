@extends('layouts.app')

@section('content')
    @include('animals.header', [
        'title' => __(''),
        'class' => 'col-lg-7'
    ])

    <div class="container-fluid mt--7">
        <div class="row space-under">
            <div class="col-xl-10 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-6">
                                <h1>Todos animais cadastrados</h1>
                            </div>
                            <div class="col-md-6 text-right">
                                <a href="{{route('animal.register')}}" class="btn btn-success btn-sm button-round"><i class="ni ni-fat-add fa-2x icon-button-round"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div>
                            <div class="row">
                                <div class="col-3">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-name">{{ __('Animal') }}</label>
                                        <input type="text" name="st_name" id="input-name" class="form-control form-control-alternative">
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-name">{{ __('Responsável') }}</label>
                                        <input type="text" name="st_name" id="input-name" class="form-control form-control-alternative">
                                    </div>
                                </div>
                                <div class="col-3 text-left" style="margin-top: 33px">
                                    <a class="btn btn-light"><i class="fa fa-search"></i></a>
                                    <a href="" class="btn btn-secondary"><i class="fa fa-undo"></i></a>
                                </div>
                            </div>
                        </div>

                        <table class="table table-white">
                            <thead>
                            <tr>
                                <th scope="col">Ações</th>
                                <th scope="col">Animal</th>
                                <th scope="col">Responsável</th>
                                <th scope="col">Espécie</th>
                                <th scope="col">Raça</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($arrAnimal as $key => $animal)
                                <tr>
                                    <td><a href="{{route('animal.detail', ['id' => $animal->id])}}" class="text-info"><i class="fa fa-info-circle fa-2x"></i></a></td>
                                    <td>{{$animal->st_name_animal}}</td>
                                    <td>{{$animal->st_name_owner}}</td>
                                    <td>{{$animal->no_specie}}</td>
                                    <td>{{$animal->no_breed}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection