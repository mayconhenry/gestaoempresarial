<div class="tab-pane fade show active container" id="tabs-attendance" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">
    <h1>Incluir novo</h1>
    <div class="dropdown-divider"></div>
    <form method="post" action="{{ route('animal.save') }}" autocomplete="off">
        @csrf
        @method('put')
        <div class="row">
            <div class="col-3">
                <div class="form-group">
                    <label class="form-control-label" for="input-name">{{ __('Data') }}</label>
                    <input type="text" name="st_name" id="input-name" class="form-control form-control-alternative" required autofocus>
                </div>
            </div>
            <div class="col-2">
                <div class="form-group">
                    <label class="form-control-label" for="input-name">{{ __('Quantidade') }}</label>
                    <input type="text" name="st_name" id="input-name" class="form-control form-control-alternative" required>
                </div>
            </div>
            <div class="col-7">
                <div class="form-group">
                    <label class="form-control-label" for="input-name">{{ __('Produto') }}</label>
                    <input type="text" name="st_name" id="input-name" class="form-control form-control-alternative" required>
                </div>
            </div>
        </div>
        <div class="text-right">
            <a href="{{ route('animal.list') }}" class="btn btn-danger btn-sm">{{ __('Cancelar') }}</a>
            <button type="submit" class="btn btn-success btn-sm">{{ __('Incluir') }}</button>
        </div>
    </form>

    <h1 class="text-left">Todos produtos consumidos</h1>
    <table class="table table-white flex">
        <thead>
            <tr>
                <th scope="col" style="width: 150px">Data</th>
                <th scope="col" style="width: 150px">Quantidade</th>
                <th scope="col" style="width: 150px">Iten</th>
                <th scope="col" style="width: 150px">Valor</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>01/08/2019</td>
                <td>02</td>
                <td>Catéter</td>
                <td>R$ 3,50</td>
            </tr>
            <tr>
                <td>01/08/2019</td>
                <td>02</td>
                <td>Seringa</td>
                <td>R$ 2,00</td>
            </tr>
            <tr>
                <td>01/08/2019</td>
                <td>01</td>
                <td>Comprimido</td>
                <td>R$ 13,00</td>
            </tr>
        </tbody>
    </table>
</div>