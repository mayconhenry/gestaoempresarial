{{--TAB DE LEMBRETE--}}
<div class="tab-pane fade" id="tabs-schedule" role="tabpanel" aria-labelledby="tabs-icons-text-2-tab">
    <form id="form-schedule">
        <input type="hidden" id="id_animal" name="id_animal" value="{{$id}}" >
        @csrf
        @method('post')
        <div class="row">
            <div class="col-3">
                <div class="form-group">
                    <label class="form-control-label" for="input-name">{{ __('Tipo de atendimento') }}</label>
                    <select name="id_type" class="form-control">
                        <option></option>
                        @foreach($arrServiceType as $serviceType)
                            <option value={{$serviceType->id}}>{{$serviceType->st_name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-3">
                <div class="form-group">
                    <label class="form-control-label" for="input-name">{{ __('Descrição') }}</label>
                    <input type="text" name="st_note" id="input-note" class="form-control form-control-alternative" required autofocus>
                </div>
            </div>
            <div class="col-2">
                <div class="form-group">
                    <label class="form-control-label" for="input-name">{{ __('Data') }}</label>
                    <input type="text" name="dt_service" id="input-date" class="form-control form-control-alternative mask-date" required>
                </div>
            </div>
            <div class="col-2">
                <div class="form-group text-center">
                    <h5 class="form-control-label" for="input-name">{{ __('Receber aviso') }}</h5>
                    <label class="custom-toggle" style="margin-top: 12px">
                        <input name="bo_notification" type="checkbox" checked>
                        <span class="custom-toggle-slider rounded-circle"></span>
                    </label>
                </div>
            </div>

            <div class="col-2 text-left" style="margin-top: 40px">
                <button type="" class="btn btn-success btn-sm" >{{ __('Lançar') }}</button>
            </div>

        </div>
    </form>
    <table class="table table-white">
        <thead>
        <tr>
            <th scope="col" class="text-center">Notificação</th>
            <th scope="col">Data</th>
            <th scope="col">Tipo de atendimento</th>
            <th scope="col">Detalhamento</th>
            <th scope="col" class="text-center">Arquivar</th>
            <th scope="col" class="text-center">Excluir</th>
        </tr>
        </thead>
        <tbody id="animal-schedules">
        @foreach($arrSchedule as $key => $schedule)
            <tr>
                <td class="text-center">
                    @if($schedule->bo_notification)
                        <i class="ni ni-chat-round ni-2x text-info"></i>
                    @endif
                </td>
                <td>{{date('d/m/Y', strtotime($schedule->dt_service))}}</td>
                <td>{{$schedule->no_type}}</td>
                <td>{{$schedule->st_note}}</td>
                <td class="text-center">
                    <a class='archive-modal' data-id="{{$schedule->id}}">
                        <i class="ni ni-check-bold text-success ni-2x"></i>
                    </a>
                </td>
                <td class="text-center">
                    <a class='delete-modal' data-id="{{$schedule->id}}">
                        <i class="ni ni-fat-remove text-danger ni-2x"></i>
                    </a>
                </td>
            </tr>
        @endforeach
                <!-- Modal para confirmar arquivamento -->
                <div class="modal fade" id="schedule-archive-modal">
                    <div class="modal-dialog modal-dialog-centered" >
                        <div class="modal-content align-items-center">
                            <div class="modal-header" >
                                <h5 class="modal-title" >Deseja realmente arquivar o agendamento?</h5>
                            </div>
                            <div class="modal-footer">
                                <input type="hidden" name="archive_schedule">
                                <button type="button" class="btn btn-success btn-sm" data-dismiss="modal" id="button-archive-schedule">Sim</button>
                                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Não</button>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Modal para confirmar delete -->
                <div class="modal fade" id="schedule-delete-modal">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content align-items-center">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Deseja realmente apagar o agendamento?</h5>
                            </div>
                            <div class="modal-footer">
                                <input type="hidden" name="delete_schedule">
                                <button type="button" class="btn btn-success btn-sm" data-dismiss="modal" id="button-delete-schedule">Sim</button>
                                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Não</button>
                            </div>
                        </div>
                    </div>
                </div>
        </tbody>
    </table>
</div>
{{--FIM DA TAB DE LEMBRETE--}}



