<div class="tab-pane" id="tabs-attendance" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">
    <h1 class="text-left">Todas atas</h1>
    <table class="table table-white flex">
        <thead>
            <tr>
                <th scope="col" style="width: 10px"></th>
                <th scope="col" style="width: 150px">Data</th>
                <th scope="col" style="width: 150px">Hora</th>
                <th scope="col" style="width: 150px">Resumo</th>
                <th scope="col" style="width: 300px">Descrição completa</th>
                <th scope="col" style="width: 50px"></th>
            </tr>
        </thead>
        <tbody id="lista-atas">
        @foreach($arrMinutes as $key => $minute)
            <tr>
                <td>{{$key+1}}</td>
                <td>{{date('d/m/Y', strtotime($minute->dt_minute))}}</td>
                <td>{{date('h:i', strtotime($minute->dt_minute))}}</td>
                <td>{{$minute->st_abstract}}</td>
                <td>{{$minute->st_description}}</td>
                <td>
                    <a data-toggle="modal" class="open-detail-minute-modal" data-id="{{$minute->st_description}}">
                        <i class="fa fa-info-circle ni-2x text-info"></i>
                    </a>
                </td>
            </tr>
        @endforeach

        <!-- Modal para detalar ata -->
        <div class="modal fade" id="detail-minute-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header align-items-center" style="margin-top: 15px">
                        <h2 id="st_description">st_description</h2>
                    </div>
                    <div class="modal-footer  align-items-right">
                        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">sair</button>
                    </div>
                </div>
            </div>
        </div>

        </tbody>
    </table>

</div>