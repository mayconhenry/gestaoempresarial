@extends('layouts.app')

@section('content')
    @include('animals.header', [
        'title' => __(''),
        'class' => 'col-lg-7'
    ])

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-10 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <h3 class="col-12 mb-0">{{ __('Cadastro de cliente') }}</h3>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('people.save') }}" autocomplete="off">
                            @csrf
                            @method('put')

                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-name">{{ __('Nome') }}</label>
                                        <input type="text" name="st_name" id="input-name" class="form-control form-control-alternative" placeholder="{{ __('Nome') }}" required autofocus>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-pedigree">{{ __('DDD + Telefone') }}</label>
                                        <input type="text" name="nu_phone" id="input-phone" class="form-control form-control-alternative mask-cell-phone">
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-pedigree">{{ __('CPF') }}</label>
                                        <input type="text" name="co_cpf" id="input-cpf" class="form-control form-control-alternative mask-cpf">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-sexo">{{ __('Sexo') }}</label>
                                        <select name="id_sex" class="form-control">
                                            <option></option>
                                            <option value=1>Masculino</option>
                                            <option value=2>Feminino</option>
                                            <option value=3>Não informar</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-sexo">{{ __('Estado') }}</label>
                                        <select name="id_state" class="form-control">
                                            <option></option>
                                            @foreach($arrState as $state)
                                                <option value={{$state->id}}>{{$state->letter}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-sexo">{{ __('Cidade') }}</label>
                                        <select name="id_city" class="form-control">
                                            <option></option>
                                            @foreach($arrCity as $city)
                                                <option value={{$city->id}}>{{$city->title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <label class="form-control-label" for="input-pedigree">{{ __('Endereço') }}</label>
                                        <input type="text" name="st_address" id="input-addres" class="form-control form-control-alternative">
                                    </div>
                                </div>
                            </div>
                            <div class="text-right">
                                <a href="{{ route('people.list') }}" class="btn btn-danger btn-sm">{{ __('Cancelar') }}</a>
                                <button type="submit" class="btn btn-success btn-sm">{{ __('Salvar') }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection