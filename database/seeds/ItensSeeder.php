<?php

use Illuminate\Database\Seeder;

class ItensSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('itens')->insert(['id' => 1, 'nome' => 'Limpeza', 'comissao'=> 20]);
        DB::table('itens')->insert(['id' => 2, 'nome' => 'Obituração', 'comissao'=> 30]);
        DB::table('itens')->insert(['id' => 3, 'nome' => 'Canal', 'comissao'=> 50]);
        DB::table('itens')->insert(['id' => 4, 'nome' => 'Selante', 'comissao'=> 40]);
    }
}
