<?php

use Illuminate\Database\Seeder;

class OrganizacoesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('organizacoes')->insert(
            [
                'id' => 1, 
                'nome'=> 'Elétrica Bitaraes'
            ],
            [
                'id' => 2, 
                'nome'=> 'Gol de Placa'
            ],
            [
                'id' => 3, 
                'nome'=> 'Ideal'
            ]
        );
    }
}
