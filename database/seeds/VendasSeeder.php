<?php

use Illuminate\Database\Seeder;

class VendasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vendas')->insert(
            [
                'id' => 1, 
                'cliente_id' => 1, 
                'tipo_venda_id' => 1, 
                'numero_parcelas' => 3, 
                'desconto' => 5, 
                'data_compra'=> '2021-02-02',
            ],
            [
                'id' => 2, 
                'cliente_id' => 2, 
                'tipo_venda_id' => 2, 
                'numero_parcelas' => 3, 
                'desconto' => 3, 
                'data_compra'=> '2021-02-02',
            ],
            [
                'id' => 3, 
                'cliente_id' => 3, 
                'tipo_venda_id' => 2, 
                'numero_parcelas' => 0, 
                'desconto' => 10, 
                'data_compra'=> '2021-02-02',
            ],
        );
    }
}
