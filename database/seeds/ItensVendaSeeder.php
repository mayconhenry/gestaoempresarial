<?php

use Illuminate\Database\Seeder;

class ItensVendaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('itens_venda')->insert(
            [
                'id' => 1, 
                'item_id' => 1, 
                'venda_id' => 1, 
                'colaborador_id' => 1, 
                'comissao' => 20, 
                'quantidade'=> 1,
                'valor'=> 180,00,
                'custos_adicionais'=> 70,
            ],
            [
                'id' => 2, 
                'item_id' => 2, 
                'venda_id' => 1, 
                'colaborador_id' => 2, 
                'comissao' => 30, 
                'quantidade'=> 2,
                'valor'=> 310,00,
                'custos_adicionais'=> 170,
            ],
            [
                'id' => 3, 
                'item_id' => 3, 
                'venda_id' => 1, 
                'colaborador_id' => 3, 
                'comissao' => 40, 
                'quantidade'=> 3,
                'valor'=> 270,00,
                'custos_adicionais'=> 110,
                ]
        );
    }
}
