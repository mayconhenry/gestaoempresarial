<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([ClienteSeeder::class]);
        $this->call([ColaboradorSeeder::class]);
        $this->call([ItensSeeder::class]);
        $this->call([ItensVendaSeeder::class]);
        $this->call([OrganizacoesSeeder::class]);
        $this->call([TiposVendaSeeder::class]);
        $this->call([UsersTableSeeder::class]);
        $this->call([VendasSeeder::class]);
    }
}
