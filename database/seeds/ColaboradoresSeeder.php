<?php

use Illuminate\Database\Seeder;

class ColaboradoresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('colaboradores')->insert(['id' => 1, 'organizacao_id' => 1, 'nome'=> 'Maycon']);
        DB::table('colaboradores')->insert(['id' => 2, 'organizacao_id' => 1, 'nome'=> 'Paulo Ceza']);
        DB::table('colaboradores')->insert(['id' => 3, 'organizacao_id' => 1, 'nome'=> 'Henrique']);
        DB::table('colaboradores')->insert(['id' => 4, 'organizacao_id' => 2, 'nome'=> 'Katia']);
    }
}
