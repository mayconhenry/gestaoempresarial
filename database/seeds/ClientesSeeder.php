<?php

use Illuminate\Database\Seeder;

class ClientesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clientes')->insert(
            [
                'id' => 1, 
                'organizacao_id' => 1, 
                'nome'=> 'Aldo'
            ],
            [
                'id' => 2, 
                'organizacao_id' => 1, 
                'nome'=> 'Rafel'
            ],
            [
                'id' => 3, 
                'organizacao_id' => 2, 
                'nome'=> 'Heles'
            ]
        );
    }
}
