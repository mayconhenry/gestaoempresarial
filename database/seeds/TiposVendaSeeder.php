<?php

use Illuminate\Database\Seeder;

class TiposVendaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipos_venda')->insert(
            [
                'id' => 1, 
                'organizacao_id' => 1, 
                'nome' => 'Visa crédito', 
                'taxa'=> 1,3
            ],
            [
                'id' => 2, 
                'organizacao_id' => 1, 
                'nome' => 'Master Card', 
                'taxa'=> 1,7
            ],
            [
                'id' => 3, 
                'organizacao_id' => 1, 
                'nome' => 'Dinheiro', 
                'taxa'=> 0
            ],
        );
    }
}
