<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItensVenda extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('itens_venda', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('item_id');
            $table->integer('venda_id');
            $table->integer('colaborador_id');
            $table->numeric('comissao');
            $table->numeric('quantidade');
            $table->numeric('valor');
            $table->numeric('custos_adicionais');
            $table->timestamps();

            $table->foreign('item_id')->references('id')->on('itens');
            $table->foreign('venda_id')->references('id')->on('vendas');
            $table->foreign('colaborador_id')->references('id')->on('colaboradores');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('itens_venda');
    }
}
